//
//  HomeViewController.swift
//  UserSignInSingUp
//
//  Created by Kerim Çağlar on 15/03/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet var kapakFoto: UIImageView!
    
    @IBOutlet var emailValue: UILabel!

    @IBOutlet var sifreValue: UILabel!
    
    var email = String()
    var sifre = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        emailValue.text = email
        sifreValue.text = sifre
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func resimYukle(_ sender: Any) {
        
        let resim = UIImagePickerController()
        resim.delegate = self
        resim.sourceType = UIImagePickerControllerSourceType.photoLibrary
        resim.allowsEditing = false
        
        self.present(resim,animated: true)
        {
            
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            kapakFoto.image = image
        }
        
        else
        {
            ///
        }
        
        self.dismiss(animated: true) { 
            //
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
