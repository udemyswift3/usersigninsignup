//
//  SecondViewController.swift
//  UserSignInSingUp
//
//  Created by Kerim Çağlar on 12/03/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    @IBOutlet weak var nameField: UITextField!
    
    @IBOutlet weak var lastNameField: UITextField!
    
    @IBOutlet weak var emailField: UITextField!
    
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var passwordAgainField: UITextField!
    
    @IBAction func SaveButton(_ sender: Any)
    {
        registerUser(urlString: "http://localhost:8888/kaydet.php")
        
    }
    
    
    func registerUser(urlString:String)
    {
        let urlRequest = URL(string: urlString)
        var request = URLRequest(url: urlRequest! as URL)
        
        request.httpMethod = "POST"
        
        let name = nameField.text
        let lastName = lastNameField.text
        let email = emailField.text
        let pass1 = passwordField.text
        let pass2 = passwordAgainField.text
        
        let parameters = "name="+name!+"&lastName="+lastName!+"&email="+email!+"&password="+pass1!
        
        request.httpBody = parameters.data(using: String.Encoding.utf8)

        
        if(pass1 != pass2)
        {
            print("Şifre tekrarlarınız uyuşmuyor")
        }
        
        
        if((name?.isEmpty)! || (lastName?.isEmpty)! || (email?.isEmpty)! || (pass1?.isEmpty)! || (pass2?.isEmpty)!)
        {
            print("Hata! Lütfen boş alan bırakmayınız")
        }
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            
            if error != nil // Hata boş değilse, HATA VARSA
            {
                print(error!)
            }
                
            else
            {
                do
                {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                    
                    if let JsonParse = json{
                        
                        var message:String!
                        var status:String!
                        
                        message = JsonParse["mesaj"] as! String?
                        status = JsonParse["durum"] as! String?
                        
                        if(status == "basarili")
                        {
                            print(message)
                            
                            DispatchQueue.main.async {
                                self.nameField.text = ""
                                self.lastNameField.text = ""
                                self.emailField.text = ""
                                self.passwordField.text = ""
                                self.passwordAgainField.text = ""
                            }
                        }
                        
                        else
                        {
                            print(message)
                        }
                        


                    }
                    
                }
                    
                catch
                {
                    print(error)
                }
            }
        }
        
        task.resume()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

