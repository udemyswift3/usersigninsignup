//
//  FirstViewController.swift
//  UserSignInSingUp
//
//  Created by Kerim Çağlar on 12/03/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    
    @IBOutlet weak var emailField: UITextField!
    
    @IBOutlet weak var passwordField: UITextField!

    @IBAction func loginButton(_ sender: Any) {
        
        loginUser(urlString: "http://localhost:8888/login.php")
    }
    
    
    func loginUser(urlString:String)
    {
        let urlRequest = URL(string: urlString)
        var request = URLRequest(url: urlRequest! as URL)
        
        request.httpMethod = "POST"

        let email = emailField.text
        let pass1 = passwordField.text
        
        let parameters = "email="+email!+"&password="+pass1!
        
        request.httpBody = parameters.data(using: String.Encoding.utf8)

        
        
        if((email?.isEmpty)! || (pass1?.isEmpty)!)
        {
            print("Hata! Lütfen boş alan bırakmayınız")
        }
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            
            if error != nil // Hata boş değilse, HATA VARSA
            {
                print(error!)
            }
                
            else
            {
                do
                {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                    
                    if let JsonParse = json{
                        
                        var message:String!
                        var status:String!
                        
                        message = JsonParse["mesaj"] as! String?
                        status = JsonParse["durum"] as! String?
                        
                        if(status == "basarili")
                        {
                            print(message)
                            
                                DispatchQueue.main.async {
                                
                                let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let homeVC = storyboard.instantiateViewController(withIdentifier: "HomePage") as! HomeViewController
                                    
                                   homeVC.email = self.emailField.text!
                                   homeVC.sifre = self.passwordField.text!
                                
                                self.present(homeVC, animated:true, completion: nil)
                                
                                self.emailField.text = ""
                                self.passwordField.text = ""
                            }
                        }
                            
                        else
                        {
                            print(message)
                        }
                        
                        
                        
                    }
                    
                }
                    
                catch
                {
                    print(error)
                }
            }
        }
        
        task.resume()

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

